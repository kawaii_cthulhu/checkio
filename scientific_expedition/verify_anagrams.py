def verify_anagrams(first_word, second_word):
    first_word = first_word.lower().replace(' ', '')
    second_word = list(second_word.lower().replace(' ', ''))
    print(first_word, second_word)
    for c in first_word:
        if c not in second_word:
            return False
        if c in second_word:
            second_word.remove(c)
    return not second_word

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert isinstance(verify_anagrams("a", 'z'), bool), "Boolean!"
    assert verify_anagrams("Programming", "Gram Ring Mop") == True, "Gram of code"
    assert verify_anagrams("Hello", "Ole Oh") == False, "Hello! Ole Oh!"
    assert verify_anagrams("Kyoto", "Tokyo") == True, "The global warming crisis of 3002"
    assert verify_anagrams("Kings Lead Hat", "Talking Heads") == True, 'test1'
    assert verify_anagrams("The Morse Code", "There Come Dots") == False, 'test2'
