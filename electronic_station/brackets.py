BRACKETS = ['(', ')', '[', ']', '{', '}']


def checkio(expression):
    curly_open = 0
    round_open = 0
    brackets_open = 0
    last_open = []
    expression = ''.join(c for c in expression if c in BRACKETS)
    for bracket in expression:
        if bracket == '(':
            round_open += 1
            last_open.append('(')
        elif bracket == ')':
            if last_open and last_open[-1] == '(':
                round_open -= 1
                last_open.pop()
            else:
                return False
        elif bracket == '[':
            brackets_open += 1
            last_open.append('[')
        elif bracket == ']':
            if last_open and last_open[-1] == '[':
                brackets_open -= 1
                last_open.pop()
            else:
                return False
        elif bracket == '{':
            curly_open += 1
            last_open.append('{')
        elif bracket == '}':
            if last_open and last_open[-1] == '{':
                curly_open -= 1
                last_open.pop()
            else:
                return False

    if curly_open or round_open or brackets_open:
        return False

    return True


# These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio("((5+3)*2+1)") == True, "Simple"
    assert checkio("{[(3+1)+2]+}") == True, "Different types"
    assert checkio("(3+{1-1)}") == False, ") is alone inside {}"
    assert checkio("[1+1]+(2*2)-{3/3}") == True, "Different operators"
    assert checkio("(({[(((1)-2)+3)-3]/3}-3)") == False, "One is redundant"
    assert checkio("2+3") == True, "No brackets, no problem"
