def longest_palindromic(text):
    palindroms = []
    for i in range(len(text)):
        for j in range(min(len(text) - i, i) + 1):
            if text[i-j-1:i] == text[i+1:i+j+2][::-1]:
                palindroms.append(text[i-j-1:i+j+2])
            if text[i-j:i] == text[i:i+j][::-1]:
                palindroms.append(text[i-j:i+j])
    lengths = list(map(len, palindroms))
    return palindroms[lengths.index(max(lengths))] or text[0]

if __name__ == '__main__':
    assert longest_palindromic("artrartrt") == "rtrartr", "The Longest"
    assert longest_palindromic("abacada") == "aba", "The First"
    assert longest_palindromic("aaaa") == "aaaa", "The A"
    assert longest_palindromic("abc") == "a", "test1"
