def checkio(number):
    roman_number = ''

    for i in range(number // 1000):
        roman_number += 'M'
    number %= 1000

    if number % 1000 >= 900:
        roman_number += 'CM'
        number -= 900

    for i in range(number // 500):
        roman_number += 'D'
    number %= 500

    if number >= 400:
        roman_number += 'CD'
    else:
        for i in range(number // 100):
            roman_number += 'C'
    number %= 100

    if number >= 90:
        roman_number += 'XC'
    else:
        for i in range(number // 50):
            roman_number += 'L'
        number %= 50

        if number >= 40:
            roman_number += 'XL'
        else:
            for i in range(number // 10):
                roman_number += 'X'
    number %= 10

    if number == 9:
        roman_number += 'IX'
        return roman_number
    for i in range(number // 5):
        roman_number += 'V'
    number %= 5
    if number == 4:
        roman_number += 'IV'
        return roman_number
    for i in range(number):
        roman_number += 'I'
    return roman_number


if __name__ == '__main__':
    # These "asserts" using only for self-checking and not necessary for auto-testing
    assert checkio(6) == 'VI', '6'
    assert checkio(76) == 'LXXVI', '76'
    assert checkio(499) == 'CDXCIX', '499'
    assert checkio(3888) == 'MMMDCCCLXXXVIII', '3888'