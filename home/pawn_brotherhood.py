def safe_pawns(pawns):
    safe_squares = []
    for pawn in pawns:
        file = int(pawn[1]) + 1
        if file > 8:
            continue
        file = str(file)

        left = chr(ord(pawn[0]) - 1)
        right = chr(ord(pawn[0]) + 1)

        if left >= 'a':
            safe_squares.append(left + file)
        if right <= 'h':
            safe_squares.append(right + file)

    safe_pawns = 0
    for pawn in pawns:
        if pawn in safe_squares:
            safe_pawns += 1

    return safe_pawns


if __name__ == '__main__':
    # These "asserts" using only for self-checking and not necessary for auto-testing
    assert safe_pawns({"b4", "d4", "f4", "c3", "e3", "g5", "d2"}) == 6
    assert safe_pawns({"b4", "c4", "d4", "e4", "f4", "g4", "e5"}) == 1
