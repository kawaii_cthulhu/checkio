CIPHER = list('ADFGVX')
CIPHER_LEN = len(CIPHER)


def encode(message, secret_alphabet, keyword):
    message = ''.join([l for l in message if l.isalpha() or l.isdigit()]).lower()
    fract = []
    for char in message:
        index = secret_alphabet.index(char)
        fract += CIPHER[index // CIPHER_LEN]
        fract += CIPHER[index % CIPHER_LEN]
    keyword = list(dict.fromkeys(list(keyword)).keys())
    key_table = [[char] for char in keyword]
    for index in range(len(key_table)):
        key_table[index].extend([fract[i] for i in range(len(fract))
                                 if i % len(key_table) == index])
    key_table = sorted(key_table)
    message = ''.join(''.join(row[1:]) for row in key_table)
    return message


def decode(message, secret_alphabet, keyword):
    keyword = list(dict.fromkeys(list(keyword)).keys())
    sorted_keyword = sorted(keyword)
    short_columns = len(keyword) - len(message) % len(keyword)
    col_len = len(message) // len(keyword)
    columns_len = {}
    for i, l in enumerate(keyword):
        if i < len(keyword) - short_columns:
            columns_len[l] = col_len + 1
        else:
            columns_len[l] = col_len
    columns = {}
    pos = 0
    for l in sorted_keyword:
        columns[l] = message[pos:pos + columns_len[l]]
        pos += columns_len[l]
    table = []
    for i in keyword:
        table.append(i + columns[i])
    message = ''
    for i in range(col_len + 2):
        for col in table:
            message += col[i + 1] if i + 1 < len(col) else ''
    decoded = ''
    for i in range(0, len(message), 2):
        row = CIPHER.index(message[i])
        col = CIPHER.index(message[i + 1])
        idx = CIPHER_LEN * row + col
        decoded += secret_alphabet[idx]
    return decoded


if __name__ == '__main__':
    assert encode("I am going",
                  "dhxmu4p3j6aoibzv9w1n70qkfslyc8tr5e2g",
                  "cipher") == 'FXGAFVXXAXDDDXGA', "encode I am going"
    assert decode("FXGAFVXXAXDDDXGA",
                  "dhxmu4p3j6aoibzv9w1n70qkfslyc8tr5e2g",
                  "cipher") == 'iamgoing', "decode I am going"
    assert encode("attack at 12:00 am",
                  "na1c3h8tb2ome5wrpd4f6g7i9j0kjqsuvxyz",
                  "privacy") == 'DGDDDAGDDGAFADDFDADVDVFAADVX', "encode attack"
    assert decode("DGDDDAGDDGAFADDFDADVDVFAADVX",
                  "na1c3h8tb2ome5wrpd4f6g7i9j0kjqsuvxyz",
                  "privacy") == 'attackat1200am', "decode attack"
    assert encode("ditiszeergeheim",
                  "na1c3h8tb2ome5wrpd4f6g7i9j0kjqsuvxyz",
                  "piloten") == 'DFGGXXAAXGAFXGAFXXXGFFXFADDXGA', "encode ditiszeergeheim"
    assert decode("DFGGXXAAXGAFXGAFXXXGFFXFADDXGA",
                  "na1c3h8tb2ome5wrpd4f6g7i9j0kjqsuvxyz",
                  "piloten") == 'ditiszeergeheim', "decode ditiszeergeheim"
    assert encode("I am going",
                  "dhxmu4p3j6aoibzv9w1n70qkfslyc8tr5e2g",
                  "weasel") == 'DXGAXAAXXVDDFGFX', "encode weasel == weasl"
    assert decode("DXGAXAAXXVDDFGFX",
                  "dhxmu4p3j6aoibzv9w1n70qkfslyc8tr5e2g",
                  "weasel") == 'iamgoing', "decode weasel == weasl"
