VOWELS = "aeiouy"

def translate(phrase):
    for v in VOWELS:
        phrase =phrase.replace(v * 3, v)
    phrase = list(phrase)
    remove = []
    for i in range(len(phrase)):
        if phrase[i] not in VOWELS and phrase[i].isalpha():
            remove.append(i + 1)
    for index in remove[::-1]:
        phrase.pop(index)
    phrase = ''.join(phrase)
    return phrase

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert translate("hieeelalaooo") == "hello", "Hi!"
    assert translate("hoooowe yyyooouuu duoooiiine") == "how you doin", "Joey?"
    assert translate("aaa bo cy da eee fe") == "a b c d e f", "Alphabet"
    assert translate("sooooso aaaaaaaaa") == "sos aaa", "Mayday, mayday"
